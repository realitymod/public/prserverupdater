using System.Text.Json;
using Spectre.Console;

namespace Updater;

internal class RealityMod
{
    private const string CdnBaseUrl = "https://c4e3967df0bda9d24c60-efd86ecb006e0afc294f130a65d5299f.ssl.cf2.rackcdn.com/";

    

    public static async Task<Version?> LatestVersion()
    {
        var url = $"{CdnBaseUrl}/version.json";

        try
        {
            var json = await HttpUtils.GetJson(url);
            var info = JsonSerializer.Deserialize(json, SourceGenerationContext.Default.LatestInfo);

            if (info is null)
            {
                return null;
            }

            return info.Latest;
        }
        catch (Exception e)
        {
            throw new ExitException("Unable to get latest version info", ExitCodes.ERROR_DOWNLOAD_LATEST_VERSION, e);
        }
    }

    

    public static async Task<List<PatchInfo>> DownloadVersionList(Version latestVersion, Version currentVersion)
    {
        List<PatchInfo> patches = new List<PatchInfo>();

        Version version = latestVersion;

        await AnsiConsole.Status()
            .StartAsync("Downloading patch info...", async ctx =>
            {
                while (true)
                {
                    string url = $"{CdnBaseUrl}/patch_{version}.json";

                    var json  = await HttpUtils.GetJson(url);
                    var info = JsonSerializer.Deserialize(json, SourceGenerationContext.Default.PatchInfo);

                    if (info == null)
                    {
                        throw new Exception("Failed to download patch info for version " + version);
                    }

                    info.ToVersion = version;

                    patches.Add(info);

                    if (info.Requires == currentVersion)
                    {
                        break;
                    }

                    version = info.Requires;
                }
            });

        patches.Reverse();

        return patches;
    }
}