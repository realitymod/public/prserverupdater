using Spectre.Console;
using Spectre.Console.Cli;
using System.ComponentModel;

namespace Updater;

internal sealed class CommandUpdate : AsyncCommand<CommandUpdate.Settings>
{
    public sealed class Settings : CommandSettings
    {
        [Description("PRBF2 server directory. If not set, command assumes it is run from `mods/pr/bin` directory.")]
        [CommandOption("--prbf2-dir")]
        public string? PRBF2Path { get; set; }

        [Description("Patches download directory. Uses `PR_DOWNLOAD_DIR` environment variable if not set. Fallbacks to OS temp directory or current directory.")]
        [CommandOption("--download-dir")]
        public string? DownloadDir { get; set; }
    }

    public override async Task<int> ExecuteAsync(CommandContext context, Settings settings)
    {
        PRBF2Directory prbf2Dir;
        try
        {
            if (settings.PRBF2Path is null)
                settings.PRBF2Path = Directories.GetBF2RootDirectory();
            settings.PRBF2Path = Path.GetFullPath(settings.PRBF2Path);

            if (settings.DownloadDir is null)
                settings.DownloadDir = Directories.GetDownloadDirectory();
            settings.DownloadDir = Path.GetFullPath(settings.DownloadDir);

            prbf2Dir = new PRBF2Directory(settings.PRBF2Path);
        }
        catch (Exception e)
        {
            AnsiConsole.WriteException(e);
            return (int)ExitCodes.ERROR_ARGUMENTS;
        }

        PrintHeader(settings);

        Dictionary<string, string> importantFiles;
        try
        {
            List<PatchInfo> versionList = await VersionList(prbf2Dir);
            if (versionList.Count == 0)
                return (int)ExitCodes.SUCCESS;

            List<DownloadedPatch> downloadedPatches = await DownloadPatches(versionList, settings.DownloadDir);

            importantFiles = Patcher.ApplyPatches(prbf2Dir, downloadedPatches.Select(patch => (patch.Path, (Patch)Patcher.LoadData(patch.Path)!)).ToList());
        }
        catch (ExitException e)
        {
            AnsiConsole.WriteException(e);
            return (int)e.ExitCode;
        }
        catch (Exception e)
        {
            AnsiConsole.WriteException(e);
            return (int)ExitCodes.ERROR_UNKNOWN;
        }

        PrintFooter(importantFiles);

        return 0;
    }

    private void PrintHeader(Settings settings)
    {
        AnsiConsole.MarkupLine($"[white]Project Reality: BF2 Server Updater[/]");
        AnsiConsole.MarkupLine($"{"".PadLeft(4)}Server root path: [cyan]{settings.PRBF2Path}[/]");
        AnsiConsole.MarkupLine($"{"".PadLeft(4)}Download path: [cyan]{settings.DownloadDir}[/]");
    }

    public static void PrintFooter(Dictionary<string, string> importantFiles)
    {
        if (importantFiles.Count == 0)
            return;

        AnsiConsole.MarkupLine("[teal]The following files were updated that may have contained server specific information:[/]");
        foreach (KeyValuePair<string, string> pair in importantFiles)
            AnsiConsole.MarkupLine($"[teal]  {pair.Key}[/]");
        AnsiConsole.MarkupLine("[teal]The original file(s) have been saved in the same directory with a [/][aqua].original[/][teal] file extension.[/]");
        AnsiConsole.MarkupLine("[teal]Please make sure any additional modifications to these files are based off the latest version.[/]");
    }

    private async Task<List<PatchInfo>> VersionList(PRBF2Directory prbf2Dir)
    {
        Version currentVersion;
        Version? latestVersion;

        try
        {
            AnsiConsole.WriteLine("Reading current version...");
            currentVersion = prbf2Dir.CurrentVersion();
        }
        catch (Exception e)
        {
            throw new ExitException("Failed to read current version", ExitCodes.ERROR_INVALID_CURRENT_VERSION, e);
        }

        AnsiConsole.MarkupLine($"[white]Current version: {currentVersion.ToString()}[/]");

        AnsiConsole.WriteLine("Checking latest version...");
        latestVersion = await RealityMod.LatestVersion();
        if (latestVersion == null || latestVersion <= currentVersion)
        {
            AnsiConsole.MarkupLine("[white]No new version available[/]");
            return new List<PatchInfo>();
        }

        AnsiConsole.MarkupLine($"[lime]New version {latestVersion.ToString()} available![/]");

        try
        {
            return await RealityMod.DownloadVersionList(latestVersion, currentVersion);
        }
        catch (Exception e)
        {
            throw new ExitException("Unable to get version information\nNo update route exists!", ExitCodes.ERROR_VERSION_NO_ROUTE, e);
        }
    }

    private sealed class DownloadedPatch
    {
        public string Path { get; set; }
        public PatchInfo Info { get; set; }

        public DownloadedPatch(string path, PatchInfo info)
        {
            Path = path;
            Info = info;
        }
    }

    private async Task<List<DownloadedPatch>> DownloadPatches(List<PatchInfo> versionList, string downloadDir)
    {
        List<DownloadedPatch> downloadedPatches = new List<DownloadedPatch>();

        List<PatchInfo> toDownload = new List<PatchInfo>();

        foreach (PatchInfo info in versionList)
        {
            string filename = Path.GetFileName(info.ServerData[0]);
            string filePath = Path.Combine(downloadDir, filename);
            AnsiConsole.Write(filename);
            if (!File.Exists(filePath) || HashUtils.ComputeSha1(filePath) != info.ServerHash)
            {
                AnsiConsole.MarkupLine(" - [red]Missing[/]");
                toDownload.Add(info);
            }
            else
            {
                AnsiConsole.MarkupLine(" - [green]Downloaded[/]");
                downloadedPatches.Add(new DownloadedPatch(filePath, info));
            }
        }

        if (toDownload.Count == 0)
            return downloadedPatches;

        TaskDescriptionColumn taskDescriptionColumn = new TaskDescriptionColumn();
        taskDescriptionColumn.Alignment = Justify.Left;

        await AnsiConsole.Progress()
            .Columns(new ProgressColumn[]
            {
                new SpinnerColumn(),
                taskDescriptionColumn,
                new TransferSpeedColumn(),
                new DownloadedColumn(),
                new RemainingTimeColumn(),
                new ProgressBarColumn(),
                new PercentageColumn(),
            })
            .StartAsync(async ctx =>
            {
                Multibar multibar = new Multibar(ctx, toDownload.Count, toDownload.Sum(info => info.ServerSize));
                var progress = new Progress<int>(i => multibar.Increment(i));

                foreach (PatchInfo info in toDownload)
                {
                    string patchUrl = info.ServerData[new Random().Next(info.ServerData.Count)];

                    string filePath = Path.Combine(downloadDir, Path.GetFileName(patchUrl));

                    multibar.NewTask(Path.GetFileName(patchUrl), info.ServerSize);

                    try
                    {
                        await Downloader.DownloadFile(patchUrl, filePath, progress);
                    }
                    catch (Exception e)
                    {
                        throw new ExitException("Failed to download update", ExitCodes.ERROR_DOWNLOAD_UPDATE, e);
                    }

                    multibar.CompleteItem();

                    downloadedPatches.Add(new DownloadedPatch(filePath, info));
                }
            });

        return downloadedPatches.OrderBy(patch => patch.Info.ToVersion).ToList();
    }
}
