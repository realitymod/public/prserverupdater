using System.IO.Compression;
using System.Text.Json;
using ICSharpCode.SharpZipLib.GZip;
using Spectre.Console;

namespace Updater;

internal sealed class Patcher
{
    private PRBF2Directory dir;
    public Dictionary<string, string> ImportantFiles;
    private Dictionary<string, SafeArchive> modifiedArchives;

    public Patcher(PRBF2Directory _dir)
    {
        dir = _dir;
        ImportantFiles = new Dictionary<string, string>();
        modifiedArchives = new Dictionary<string, SafeArchive>();
    }

    public static Patch? LoadData(string patchFilePath)
    {
        using ZipArchive zip = ZipFile.OpenRead(patchFilePath);
        ZipArchiveEntry? info = zip.GetEntry("info.json");

        if (info == null)
        {
            return null;
        }

        using var stream = info.Open();

        return JsonSerializer.Deserialize(stream, SourceGenerationContext.Default.Patch);
    }

    public static void ValidateVersion(Patch patch, Version currentVersion)
    {
        if (currentVersion != patch.RequiresVersion)
        {
            throw new ExitException($"Patch requires version {patch.RequiresVersion}, but current version is {currentVersion}", ExitCodes.ERROR_VERSION_MISMATCH);
        }
    }

    public static Dictionary<string, string> ApplyPatches(PRBF2Directory prbf2Dir, List<(string, Patch)> patches)
    {
        Patcher patcher = new Patcher(prbf2Dir);

        TaskDescriptionColumn taskDescriptionColumn = new TaskDescriptionColumn();
        taskDescriptionColumn.Alignment = Justify.Left;
        AnsiConsole.Progress()
            .Columns(new ProgressColumn[]
            {
                new SpinnerColumn(),
                new TotalColumn(),
                new ProgressBarColumn(),
                new PercentageColumn(),
                taskDescriptionColumn,
            })
        .Start(ctx =>
        {
            int totalOps = patches.Sum(p => p.Item2.PatchData.Length);
            // Add the total number of patches to the total operations for cleanup operations
            totalOps += patches.Count;
            Multibar multibar = new Multibar(ctx, patches.Count, totalOps);

            foreach ((string path, Patch patch) in patches)
            {
                multibar.NewTask(patch.Version.ToString(), patch.PatchData.Length+1);

                string lockPath = Path.Combine(prbf2Dir.ModDir, "update.lock");

                if (File.Exists(lockPath))
                {
                    throw new ExitException("Another update is in progress. Please wait for it to finish. If no update is pending, remove lock file in `mods/pr/update.lock`", ExitCodes.ERROR_PENDING_UPDATE);
                }

                ValidateVersion(patch, prbf2Dir.CurrentVersion());

                using (StreamWriter sw = new StreamWriter(lockPath))
                    sw.WriteLine(path);

                patcher.ApplyPatch(patch, path, multibar);

                prbf2Dir.SetCurrentVersion(patch.Version);

                File.Delete(lockPath);

                multibar.AppendCurrentTaskDescription("Success!");
                multibar.CompleteItem();
            }
        });

        return patcher.ImportantFiles;
    }

    public void ApplyPatch(Patch patch, string patchFilePath, Multibar multibar)
    {
        using ZipArchive zip = ZipFile.OpenRead(patchFilePath);

        foreach (PatchItem patchItem in patch.PatchData)
        {
            ApplyPatchItem(patchItem, zip, multibar);
        }

        multibar.AppendCurrentTaskDescription("Moving files and cleaning up - if you quit now server files might be broken.");

        foreach (KeyValuePair<string, SafeArchive> archive in modifiedArchives)
        {
            archive.Value.SafeMove();
            archive.Value.Cleanup();
        }

        modifiedArchives.Clear();

        multibar.Increment(1);
    }

    private const string _modDir = "@ModDir";
    private const string _rootDir = "@RootDir";
    private const string _patchOriginalExtension = ".original";

    private (string, string) GetDestinationPaths(string destination)
    {
        string shortDestination = destination;

        if (destination.StartsWith(_modDir))
        {
            shortDestination = destination.Substring(_modDir.Length + 1);
            destination = destination.Replace(_modDir, dir.ModDir);
        }
        else if (destination.StartsWith(_rootDir))
        {
            shortDestination = destination.Substring(_rootDir.Length + 1);
            destination = destination.Replace(_rootDir, dir.RootDir);
        }

        return (destination, shortDestination);
    }

    public void ApplyPatchItem(PatchItem item, ZipArchive zip, Multibar multibar)
    {
        (string destination, string shortDestination) = GetDestinationPaths(item.Destination);

        if (item.IsImportantFile == true && Path.Exists(destination))
        {
            if (!ImportantFiles.ContainsKey(shortDestination))
            {
                string original = string.Concat(destination, _patchOriginalExtension);
                if (!File.Exists(original))
                {
                    File.Copy(destination, original);
                    ImportantFiles.Add(shortDestination, string.Concat(shortDestination, _patchOriginalExtension));
                }
            }
        }

        multibar.AppendCurrentTaskDescription(MethodDescription(item.Method, shortDestination));
        multibar.Refresh();

        switch (item.Method)
        {
            case PatchMethod.FileAdd:
            case PatchMethod.FileModify:
            case PatchMethod.FileRemove:
                ApplyPatchFile(item, zip, destination);
                break;
            case PatchMethod.ZipAdd:
            case PatchMethod.ZipModify:
            case PatchMethod.ZipRemove:
                ApplyPatchZip(item, zip, destination, shortDestination);
                break;
        }

        multibar.Increment(1);
    }

    private void ApplyPatchFile(PatchItem item, ZipArchive zip, string destination)
    {
        switch (item.Method)
        {
            case PatchMethod.FileAdd:
                ApplyPatchFileAdd(destination, ReadZipEntry(zip, item.Source!));
                break;
            case PatchMethod.FileModify:
                ApplyPatchFileModify(destination, ReadZipEntry(zip, item.Source!), item.BeforeHash!, item.AfterHash!);
                break;
            case PatchMethod.FileRemove:
                ApplyPatchFileRemove(destination, item.IsDirectory);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(item), $"Unknown PathMethod {item.Method}");
        }

    }

    private void ApplyPatchZip(PatchItem item, ZipArchive zip, string destination, string shortDestination)
    {
        var itemEntry = item.Entry!.ToLowerInvariant();
        switch (item.Method)
        {
            case PatchMethod.ZipAdd:
                ApplyPatchZipAdd(destination, itemEntry, ReadZipEntry(zip, item.Source!), shortDestination);
                break;
            case PatchMethod.ZipModify:
                ApplyPatchZipModify(destination, itemEntry, ReadZipEntry(zip, item.Source!), shortDestination,
                    item.BeforeHash!, item.AfterHash!);
                break;
            case PatchMethod.ZipRemove:
                ApplyPatchZipRemove(destination, itemEntry, shortDestination);
                break;

            default:
                throw new ArgumentOutOfRangeException(nameof(item), $"Unknown PathMethod {item.Method}");
        }
    }

    private string MethodDescription(PatchMethod method, string shortDestination)
    {
        string color = "";
        switch (method)
        {
            case PatchMethod.FileAdd:
            case PatchMethod.ZipAdd:
                color = "green";
                break;
            case PatchMethod.FileModify:
            case PatchMethod.ZipModify:
                color = "olive";
                break;
            case PatchMethod.FileRemove:
            case PatchMethod.ZipRemove:
                color = "maroon";
                break;
        }

        return $"[{color}]{method}:[/] {shortDestination}";
    }

    private void ApplyPatchFileAdd(string destination, Stream source)
    {
        SafeFile file = new SafeFile(destination);

        DirectoryInfo? parent = Directory.GetParent(destination);
        if (parent != null && !parent.Exists)
        {
            parent.Create();
        }

        using (FileStream fs = File.Open(file.TempPath, FileMode.Create))
        {
            source.CopyTo(fs);
        }

        file.SafeMove();
    }

    private void ApplyPatchFileModify(string destination, Stream source, string beforeHash, string afterHash)
    {
        if (!File.Exists(destination))
        {
            return;
        }

        SafeFile file = new SafeFile(destination);

        if (HashUtils.ComputeSha1(destination) == beforeHash)
        {
            BSPatch.Apply(destination, file.TempPath, source);
        }

        HashUtils.ValidateHash(file.TempPath, afterHash);

        file.SafeMove();
    }

    private void ApplyPatchFileRemove(string destination, bool? isDirectory)
    {
        if (isDirectory == true && Directory.Exists(destination))
        {
            Directory.Delete(destination, true);
        }
        else if (File.Exists(destination))
        {
            File.Delete(destination);
        }
    }

    private void ApplyPatchZipAdd(string destination, string entry, Stream source, string shortDestination)
    {
        DirectoryInfo? parent = Directory.GetParent(destination);
        if (parent is { Exists: false })
        {
            parent.Create();
        }

        SafeArchive archive = modifiedArchives.GetValueOrDefault(shortDestination) ?? new SafeArchive(destination);
        modifiedArchives[shortDestination] = archive;

        archive.Archive.GetEntry(entry)?.Delete();

        ZipArchiveEntry newEntry = archive.Archive.CreateEntry(entry);

        // TODO: find a way to get the length of the stream
        // long len = source.Length;
        source.CopyTo(newEntry.Open());
        // archive.WrittenBytes(len);
    }

    private void ApplyPatchZipModify(string destination, string entry, Stream source, string shortDestination, string beforeHash, string afterHash)
    {
        SafeArchive archive = modifiedArchives.GetValueOrDefault(shortDestination) ?? new SafeArchive(destination);
        modifiedArchives[shortDestination] = archive;

        ZipArchiveEntry? zipEntry = archive.Archive.GetEntry(entry);

        if (zipEntry == null)
        {
            return;
        }

        string tempFilePathInput = Path.GetTempFileName();
        string tempFilePathOutput = Path.GetTempFileName();

        zipEntry.ExtractToFile(tempFilePathInput, true);

        if (HashUtils.ComputeSha1(tempFilePathInput) == beforeHash)
        {
            BSPatch.Apply(tempFilePathInput, tempFilePathOutput, source);
        }

        HashUtils.ValidateHash(tempFilePathOutput, afterHash);

        zipEntry.Delete();
        archive.Archive.CreateEntryFromFile(tempFilePathOutput, entry);

        File.Delete(tempFilePathInput);
        File.Delete(tempFilePathOutput);
    }

    private void ApplyPatchZipRemove(string destination, string entry, string shortDestination)
    {
        if (!File.Exists(destination))
        {
            return;
        }

        SafeArchive archive = modifiedArchives.GetValueOrDefault(shortDestination) ?? new SafeArchive(destination);
        modifiedArchives[shortDestination] = archive;

        ZipArchiveEntry? zipEntry = archive.Archive.GetEntry(entry);
        if (zipEntry == null)
        {
            return;
        }

        // TODO: find a way to get the length of entry
        // long len = zipEntry.Length;
        zipEntry.Delete();
        // archive.WrittenBytes(len);
    }


    private Stream ReadZipEntry(ZipArchive zip, string entry)
    {
        ZipArchiveEntry? zipEntry = zip.GetEntry(entry);

        if (zipEntry == null)
        {
            throw new Exception("Failed to read zip entry");
        }

        return zipEntry.Open();
    }
}
