namespace Updater;

internal class PatchInfo
{
    public required Version Requires { get; init; }
    public required int ServerSize { get; init; }
    public required string ServerHash { get; init; }
    public required List<string> ServerData { get; init; }

    // ToVersion pupulated from request
    public Version? ToVersion { get; set; }
    // DownloadPath populated after download
    public string? DownloadPath { get; init; }
}