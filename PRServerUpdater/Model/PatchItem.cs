using System.Text.Json.Serialization;

namespace Updater;

internal struct PatchItem
{
    /// <summary>Formats Entry to be compatible with Zip Archive</summary>
    private static string FormatEntry(string entry)
    {
        return string.Join("/", entry.Split('\\'));
    }

    [JsonConverter(typeof(JsonStringEnumConverter<PatchMethod>))]
    public required PatchMethod Method { get; set; }
    private string? source;
    public string? Source { get => source; set { if (value is not null) source = FormatEntry(value); } }
    private string destination;
    public required string Destination { get => destination; set { destination = FormatEntry(value); } }
    private string? entry;
    public string? Entry { get => entry; set { if (value is not null) entry = FormatEntry(value); } }
    public string? BeforeHash { get; set; }
    public string? AfterHash { get; set; }
    public bool? IsReplacementExecutable { get; set; }
    public bool? IsImportantFile { get; set; }
    public bool? IsDirectory { get; set; }
}