using System.Runtime.Serialization;

namespace Updater;

internal enum PatchMethod
{
    [EnumMember(Value = "FileAdd")]
    FileAdd,
    [EnumMember(Value = "FileModify")]
    FileModify,
    [EnumMember(Value = "FileRemove")]
    FileRemove,
    [EnumMember(Value = "ZipAdd")]
    ZipAdd,
    [EnumMember(Value = "ZipModify")]
    ZipModify,
    [EnumMember(Value = "ZipRemove")]
    ZipRemove
}