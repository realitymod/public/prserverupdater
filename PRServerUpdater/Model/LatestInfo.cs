namespace Updater;

internal sealed class LatestInfo
{
    public required Version Latest { get; init; }
}