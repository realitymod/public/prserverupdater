namespace Updater;

internal struct PatchTouchData
{
    public required DateTimeOffset Timestamp { get; set; }
    public required string[] Archives { get; set; }
}