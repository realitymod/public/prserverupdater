using System.Text.Json.Serialization;

namespace Updater;

[JsonSerializable(typeof(PatchInfo))]
[JsonSerializable(typeof(LatestInfo))]
[JsonSerializable(typeof(Patch))]
internal partial class SourceGenerationContext : JsonSerializerContext
{
}