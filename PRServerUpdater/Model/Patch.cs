namespace Updater;

internal struct Patch
{
    public required Version Version { get; set; }
    public required Version RequiresVersion { get; set; }
    public required PatchUtility[] PrePatch { get; set; }
    public required PatchUtility[] PostPatch { get; set; }
    public required PatchTouchData TouchData { get; set; }
    public required PatchItem[] PatchData { get; set; }
}