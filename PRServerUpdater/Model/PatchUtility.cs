using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Updater;

[JsonConverter(typeof(JsonStringEnumConverter<PatchUtility>))]
internal enum PatchUtility
{
    [EnumMember(Value = "AcceptNewLicense")]
    AcceptNewLicense,
    [EnumMember(Value = "ReplaceExecutables")]
    ReplaceExecutables,
    [EnumMember(Value = "DeleteShaderCache")]
    DeleteShaderCache,
}