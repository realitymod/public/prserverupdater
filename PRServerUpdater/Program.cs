﻿using System.Diagnostics.CodeAnalysis;
using Spectre.Console.Cli;

namespace Updater;

internal static class Program
{
    [DynamicDependency(DynamicallyAccessedMemberTypes.All, typeof(CommandUpdate))]
    [DynamicDependency(DynamicallyAccessedMemberTypes.All, typeof(CommandUpdate.Settings))]
    [DynamicDependency(DynamicallyAccessedMemberTypes.All, typeof(CommandApply))]
    [DynamicDependency(DynamicallyAccessedMemberTypes.All, typeof(CommandApply.Settings))]
    [DynamicDependency(DynamicallyAccessedMemberTypes.All, "Spectre.Console.Cli.ExplainCommand", "Spectre.Console.Cli")]
    [DynamicDependency(DynamicallyAccessedMemberTypes.All, "Spectre.Console.Cli.VersionCommand", "Spectre.Console.Cli")]
    [DynamicDependency(DynamicallyAccessedMemberTypes.All, "Spectre.Console.Cli.XmlDocCommand", "Spectre.Console.Cli")]
    private static async Task<int> Main(string[] args)
    {
        var app = new CommandApp<CommandUpdate>();

        app.WithDescription("Update PRBF2 server to the latest version.");

        app.Configure(config =>
        {
            config.AddCommand<CommandApply>("apply")
                .WithDescription("Apply patch files to PRBF2 server.");
        });

        return await app.RunAsync(args);
    }
}
