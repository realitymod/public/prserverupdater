namespace Updater;

internal static class Downloader
{
    public static async Task DownloadFile(string url, string path, IProgress<int> progress)
    {
        using var client = new HttpClient();

        var stream = await client.GetStreamAsync(url);

        await using FileStream file = File.Create(path);
        byte[] buffer = new byte[8192];
        int bytesRead;

        while ((bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length)) > 0)
        {
            await file.WriteAsync(buffer, 0, bytesRead);

            progress.Report(bytesRead);
        }
    }
}