using ICSharpCode.SharpZipLib.BZip2;

using System.Text;

namespace Updater;

// Ref: https://www.daemonology.net/bsdiff/
/*
File format:
  0	8	"PRDIFF10"
  8	8	X
  16	8	Y
  24	8	sizeof(newfile)
  32	X	bzip2(control block)
  32+X	Y	bzip2(diff block)
  32+X+Y	???	bzip2(extra block)
with control block a set of triples (x,y,z) meaning "add x bytes
from oldfile to x bytes from the diff block; copy y bytes from the
extra block; seek forwards in oldfile by z bytes".
*/
internal class BSPatch
{
    private struct Header
    {
        public string Topic;
        public long X;
        public long Y;
        public long NewSize;
    }

    private struct ControlHeader
    {
        public long X;
        public long Y;
        public long Z;
    }

    private const int headerSize = 32;

    public static void Apply(string input, string output, Stream patchStream)
    {
        if (patchStream.Length < headerSize)
            throw new CorruptPatchException();

        string tempFile = Path.Join(Path.GetTempPath(), Path.GetRandomFileName());

        using (FileStream fs = File.Create(tempFile))
            patchStream.CopyTo(fs);

        using Stream headerStream = File.OpenRead(tempFile);

        byte[] headerBytes = new byte[headerSize];
        int read = headerStream.Read(headerBytes, 0, headerSize);
        if (read != headerSize)
            throw new CorruptPatchException();

        Header header = new Header
        {
            Topic = Encoding.UTF8.GetString(headerBytes, 0, 8),
            X = ReadInt64(headerBytes, 8),
            Y = ReadInt64(headerBytes, 16),
            NewSize = ReadInt64(headerBytes, 24)
        };

        if (header.Topic != "PRDIFF10")
            throw new CorruptPatchException();

        if (header.X < 0 || header.Y < 0 || header.NewSize < 0)
            throw new CorruptPatchException();

        using Stream controlStream = File.OpenRead(tempFile);
        controlStream.Seek(headerSize, SeekOrigin.Begin);
        using BZip2InputStream controlBlock = new BZip2InputStream(controlStream);

        using Stream diffStream = File.OpenRead(tempFile);
        diffStream.Seek(headerSize + header.X, SeekOrigin.Begin);
        using BZip2InputStream diffBlock = new BZip2InputStream(diffStream);

        int extraIndex = headerSize + (int)header.X + (int)header.Y;
        using Stream extraStream = File.OpenRead(tempFile);
        extraStream.Seek(extraIndex, SeekOrigin.Begin);
        using BZip2InputStream extraBlock = new BZip2InputStream(extraStream);

        using FileStream inputFile = File.OpenRead(input);
        byte[] outputBuf = new byte[header.NewSize];

        // Reuse the same buffer for control header
        byte[] ctrlHeaderBytes = new byte[24];

        long oldPos = 0, newPos = 0;
        while (newPos < header.NewSize)
        {
            int readCtrl = controlBlock.Read(ctrlHeaderBytes, 0, 24);
            if (readCtrl != 24)
                throw new CorruptPatchException();

            ControlHeader control = new ControlHeader
            {
                X = ReadInt64(ctrlHeaderBytes, 0),
                Y = ReadInt64(ctrlHeaderBytes, 8),
                Z = ReadInt64(ctrlHeaderBytes, 16)
            };

            if (newPos + control.X > header.NewSize)
                throw new CorruptPatchException();

            byte[] old = new byte[control.X];
            inputFile.Seek(oldPos, SeekOrigin.Begin);
            inputFile.Read(old, 0, (int)control.X);

            int readDiff = diffBlock.Read(outputBuf, (int)newPos, (int)control.X);
            if (readDiff != control.X)
                throw new CorruptPatchException();

            for (int i = 0; i < control.X; i++)
            {
                if (oldPos + i >= 0 && oldPos + i < old.Length)
                    outputBuf[newPos + i] += old[oldPos + i];
            }

            newPos += control.X;
            oldPos += control.X;

            if (newPos + control.Y > header.NewSize)
                throw new CorruptPatchException();

            int readExtra = extraBlock.Read(outputBuf, (int)newPos, (int)control.Y);
            if (readExtra != control.Y)
                throw new CorruptPatchException();

            newPos += control.Y;
            oldPos += control.Z;
        }

        using (FileStream outputFile = File.Open(output, FileMode.Create))
        {
            outputFile.Write(outputBuf);
        }
    }

    private sealed class CorruptPatchException : Exception
    {
        private const string DefaultMessage = "Corrupt patch";
        public CorruptPatchException() : base(DefaultMessage) { }
    }


    private static long ReadInt64(byte[] buf, int offset)
    {
        long value = buf[offset + 7] & 0x7F;

        for (int index = 6; index >= 0; index--)
        {
            value *= 256;
            value += buf[offset + index];
        }

        if ((buf[offset + 7] & 0x80) != 0)
            value = -value;

        return value;
    }
}