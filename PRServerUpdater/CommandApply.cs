using Spectre.Console;
using Spectre.Console.Cli;
using System.ComponentModel;

namespace Updater;

internal sealed class CommandApply : Command<CommandApply.Settings>
{
    public sealed class Settings : CommandSettings
    {
        [CommandArgument(0, "[patch]")]
        public string[]? PatchLocation { get; set; }

        [Description("PRBF2 server directory. If not set, command assumes it is run from `mods/pr/bin` directory.")]
        [CommandOption("--prbf2-dir")]
        public string? PRBF2Path { get; set; }
    }

    public override int Execute(CommandContext context, Settings settings)
    {
        PRBF2Directory prbf2Dir;
        try
        {
            if (settings.PRBF2Path is null)
                settings.PRBF2Path = Directories.GetBF2RootDirectory();
            settings.PRBF2Path = Path.GetFullPath(settings.PRBF2Path);

            prbf2Dir = new PRBF2Directory(settings.PRBF2Path);
        }
        catch (Exception e)
        {
            AnsiConsole.WriteException(e);
            return (int)ExitCodes.ERROR_ARGUMENTS;
        }

        Dictionary<string, string> importantFiles;
        try
        {
            List<(string, Patch)> patches = settings.PatchLocation!.Select(path => (path, (Patch)Patcher.LoadData(path)!)).ToList();

            importantFiles = Patcher.ApplyPatches(prbf2Dir, patches);
        }
        catch (ExitException e)
        {
            AnsiConsole.WriteException(e);
            return (int)e.ExitCode;
        }
        catch (Exception e)
        {
            AnsiConsole.WriteException(e);
            return (int)ExitCodes.ERROR_UNKNOWN;
        }

        CommandUpdate.PrintFooter(importantFiles);

        return 0;
    }
}
