using Spectre.Console;
using Spectre.Console.Rendering;

namespace Updater;

internal sealed class Multibar
{
    private readonly ProgressContext _context;
    public ProgressTask TotalTask { get; }
    public ProgressTask? CurrentTask { get; private set; }
    
    private string? _currentTaskDescritpion;
    private readonly int _totalItems;
    private readonly int _totalDigits;
    private int _completedItems = 0;

    public Multibar(ProgressContext context, int totalItems, int maxValue)
    {
        this._context = context;
        this._totalItems = totalItems;
        this._totalDigits = totalItems.ToString().Length;

        this.TotalTask = context.AddTask(TotalDescription(), true, maxValue);
    }

    private string TotalDescription()
    {
        return $"Total ({_completedItems.ToString().PadLeft(_totalDigits)}/{_totalItems})";
    }

    public void NewTask(string description, int maxValue)
    {
        CurrentTask = _context.AddTaskBefore(description, TotalTask, true, maxValue);
        _currentTaskDescritpion = description;
    }

    public void AppendCurrentTaskDescription(string description)
    {
        CurrentTask!.Description = $"{_currentTaskDescritpion} {description}";
    }

    public void Increment(double value)
    {
        CurrentTask?.Increment(value);
        TotalTask.Increment(value);
    }

    public void CompleteItem()
    {
        _completedItems++;
        TotalTask.Description = TotalDescription();
    }

    public void Refresh()
    {
        _context.Refresh();
    }
}

internal sealed class TotalColumn : ProgressColumn
{
    public override IRenderable Render(RenderOptions options, ProgressTask task, TimeSpan span)
    {
        return new Text($"{task.Value.ToString().PadLeft(task.MaxValue.ToString().Length)}/{task.MaxValue}").Justify(Justify.Right);
    }
}