namespace Updater;

internal static class HttpUtils
{
    public static async Task<string> GetJson(string url)
    {
        using HttpClient client = new HttpClient();

        var response = await client.GetAsync(url);

        return await response.Content.ReadAsStringAsync();
    }
}

internal static class HashUtils
{
    public static void ValidateHash(string path, string expected)
    {
        var actual = ComputeSha1(path);
        if (actual != expected)
        {
            throw new Exception("The file does not match the expected hash");
        }
    }

    public static string ComputeSha1(string path)
    {
        using var fs = File.OpenRead(path);
        Span<byte> hashBuffer = new byte[20];
        System.Security.Cryptography.SHA1.HashData(fs, hashBuffer);
        return Convert.ToHexString(hashBuffer).ToLower();
    }
}
