namespace Updater;

internal sealed class ExitException : Exception
{
    public ExitCodes ExitCode { get; private set; }

    public ExitException(string message, ExitCodes exitCode, Exception? innerException) : base(message, innerException)
    {
        ExitCode = exitCode;
    }

    public ExitException(string message, ExitCodes exitCode) : base(message)
    {
        ExitCode = exitCode;
    }
}