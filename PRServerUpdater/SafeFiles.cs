using System.IO.Compression;

namespace Updater;

internal class SafeFile
{
    public string OrigPath { get; }
    public string TempPath { get => string.Concat(OrigPath, ".safetemp"); }
    private string BackupPath { get => string.Concat(OrigPath, ".safeorig"); }

    public SafeFile(string originalPath)
    {
        OrigPath = originalPath;
        if (File.Exists(OrigPath))
        {
            File.Copy(OrigPath, TempPath, true);
        }
    }

    public virtual void SafeMove()
    {
        try
        {
            if (File.Exists(OrigPath))
            {
                File.Copy(OrigPath, BackupPath);
                File.Copy(TempPath, OrigPath, true);
                File.Delete(TempPath);
            }
            else
            {
                File.Move(TempPath, OrigPath);
            }
        }
        catch (Exception e1)
        {
            try
            {
                Restore();
            }
            catch (Exception e2)
            {
                throw new Exception("Failed to restore file", e2);
            }
            throw new Exception("Failed to safely move file, file was restored", e1);
        }

        Cleanup();
    }

    private void Restore()
    {
        if (File.Exists(BackupPath))
        {
            File.Move(BackupPath, OrigPath, true);
        }
    }

    public virtual void Cleanup()
    {
        if (File.Exists(BackupPath))
        {
            File.Delete(BackupPath);
        }
        if (File.Exists(TempPath))
        {
            File.Delete(TempPath);
        }
    }
}

internal sealed class SafeArchive : SafeFile
{
    private ZipArchive? _Archive { get; set; }
    public ZipArchive Archive
    {
        get
        {
            if (_Archive == null)
            {
                _Archive = ZipFile.Open(TempPath, ZipArchiveMode.Update);
            }
            return _Archive;
        }
    }

    public SafeArchive(string originalPath) : base(originalPath) { }

    private void CloseArchive()
    {
        _Archive?.Dispose();
        _Archive = null;
    }

    public override void SafeMove()
    {
        CloseArchive();
        base.SafeMove();

    }

    public override void Cleanup()
    {
        CloseArchive();
        base.Cleanup();
    }
}
