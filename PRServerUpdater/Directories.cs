using System.Xml;
using System.Text.RegularExpressions;

namespace Updater;

internal sealed class PRBF2Directory
{
    private readonly string _path;

    public PRBF2Directory(string path)
    {
        if (!Path.IsPathRooted(path))
            path = Path.GetFullPath(path);
        _path = path;

        if (!IsValid())
        {
            throw new Exception($"\"{path} is not a valid PRBF2 directory");
        }
    }

    private string ModDesc
    {
        get => Path.Combine(ModDir, "mod.desc");
    }

    public string RootDir { get => _path; }

    public string ModDir
    {
        get => Path.Combine(_path, "mods", "pr");
    }

    public bool IsValid()
    {
        return File.Exists(ModDesc);
    }

    public Version CurrentVersion()
    {
        using FileStream modDescFile = File.OpenRead(ModDesc);

        XmlDocument doc = new XmlDocument();

        doc.Load(modDescFile);

        XmlNode? node = doc.SelectSingleNode("/mod/version");

        if (node is null)
        {
            throw new Exception("mod.desc does not contain a version node");
        }

        return new Version(node.InnerText.Trim());
    }

    public void SetCurrentVersion(Version version)
    {
        string modDescContent = File.ReadAllText(ModDesc);

        modDescContent = Regex.Replace(modDescContent, "<version>.+?</version>", $"<version>{version.ToString()}</version>");

        File.WriteAllText(ModDesc, modDescContent);
    }
}

public static class Directories
{
    public static string GetCurrentModDirectory()
    {
        string path = Path.Combine("..", "mod.desc");
        if (!File.Exists(path))
            throw new Exception("Unable to find mod directory");

        return Path.GetDirectoryName(path)!;
    }

    public static string GetBF2RootDirectory()
    {
        string modDir = GetCurrentModDirectory();
        string rootDir = Path.Combine(modDir, "..", "..");
        if (!Directory.Exists(Path.Combine(rootDir, "mods")))
        {
            throw new Exception("Unable to find root BF2 Server directory");
        }

        return rootDir;
    }

    public static string GetDownloadDirectory()
    {
        string? downloadDir = "";

        downloadDir = Environment.GetEnvironmentVariable("PR_DOWNLOAD_DIR");

        if (downloadDir == null)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Error.WriteLine("Environment variable \"PR_DOWNLOAD_DIR\" does not exist.");
            Console.ResetColor();

            try
            {
                downloadDir = Path.GetTempPath();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Error.WriteLine("Unable to get temp directory\n" + e.Message);
                Console.ResetColor();
            }
        }

        if (downloadDir == null)
        {
            downloadDir = Directory.GetCurrentDirectory();
        }

        Console.Write("Download Directory: ");
        Console.ForegroundColor = ConsoleColor.DarkCyan;
        Console.WriteLine(downloadDir);
        Console.ResetColor();

        return downloadDir;
    }
}